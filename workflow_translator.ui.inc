<?php

/**
 * @file
 * Provides Workflow Translator ui controller.
 */

/**
 * Workflow translator ui controller.
 */
class WorkflowUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    // something similar to the block page
    // regions = states
    // blocks  = services

    $unconfigured = $translator->getController()->isAvailable($translator, TRUE);
    if (is_array($unconfigured)) {
      foreach ($unconfigured as $service) {
        drupal_set_message(t('Service %serviceName is not yet configured.', array('%serviceName' => $service['label'])), 'warning');
      }
    }
    else if (!$unconfigured) {
      drupal_set_message(t('One or more services are not yet configured.'), 'warning');
    }

    $form['services'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Select Services'),
      '#description' => t('Activate and Order the services. Translators only can be in Translator. A translator must be defined in TMGMT.'),
      '#tree'        => TRUE,
      '#theme'       => 'workflow_translator_service_table',
      '#element_validate' => array('workflow_translator_service_table_validate'),
    );

    $states = array(
      WORKFLOW_TRANSLATOR_BEFORE_TRANSLATION => t('before Translation'),
      WORKFLOW_TRANSLATOR_AFTER_TRANSLATION => t('after Translation'),
    );

    $active_services = workflow_translator_get_active_services($translator);
    $tmp = array();
    foreach ($active_services as $state => $services) {
      $tmp += $services;
    }
    $active_services = $tmp;

    $form['services']['job_states'] = array(
      '#type' => 'value',
      '#value' => $states,
    );

    $form['services']['service_list'] = array();

    $all_services = workflow_translator_get_all_services();
    $weight_delta = round(count($all_services) / 2);

    foreach ($all_services as $key => $service_array) {
      $service = array();

      if (isset($service_array['translator']) && $service_array['translator'] instanceof TMGMTTranslator) {
        $name = t('Translator') . ': ' . $service_array['translator']->label;
        $options_state = array(WORKFLOW_TRANSLATOR_TRANSLATOR => t('Translator'));
        unset($options_state[3]);
        $service['translator'] = array(
          '#type'  => 'value',
          '#value' => 1,
        );
      }
      else {
        $name = $service_array['label'];
        $options_state = $states;
      }

      $service['name'] = array(
        '#markup' => check_plain($name),
      );

      $service['weight'] = array(
        '#type' => 'weight',
        '#default_value' => isset($active_services[$key]['weight']) ? $active_services[$key]['weight'] : 0,
        '#delta' => $weight_delta,
        '#title_display' => 'invisible',
        '#title' => t('Weight for @service service', array('@service' => $name)),
      );
      $service['state'] = array(
        '#type' => 'select',
        '#default_value' => isset($active_services[$key]['state']) ? $active_services[$key]['state'] : WORKFLOW_TRANSLATOR_DISABLED,
        '#empty_value' => -1,
        '#title_display' => 'invisible',
        '#title' => t('Region for @service service', array('@service' => $name)),
        '#options' => $options_state + array(WORKFLOW_TRANSLATOR_DISABLED => t('Disabled')),
      );

      $form['services']['service_list'][$key] = $service;
    }

    // add settings form of activated plugins
    $form['service_settings'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Service Settings'),
      '#description' => t('Settings for each service.'),
      '#tree'        => TRUE,
    );

    foreach ($active_services as $key => $service) {

      if (isset($service['translator'])) {
        $form['service_settings'][$key] = array(
          '#type'   => 'item',
          '#markup' => t('Settings for the !translator are available through the !form', array(
            '!translator' => $service['translator']->label,
            '!form'       => l(t('translator settings form'), 'admin/config/regional/tmgmt/translators/manage/'. $service['translator']->name),
          )),
        );
      }
      else {
        /** @var $class WorkflowTranslationDefaultUIServiceInterface */
        $class = _tmgmt_plugin_controller('workflow', $key, 'ui');
        $tmp = $class->settingsForm(array(), $form_state, $translator);
        if (!empty($tmp)) {
          $form['service_settings'][$key] = $tmp;
        }
      }

    }

    return $form;
  }

  /**
   * Implements TMGMTTranslatorUIControllerInterface::checkoutSettingsForm().
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {
    $active_services = workflow_translator_get_active_services($job->getTranslator());
    foreach ($active_services as $services) {
      foreach($services as $key => $service) {
        if (isset($service['translator']) && !empty($service['translator']->plugin)) {
          /** @var $ui_controller TMGMTTranslatorUIController */
          $ui_controller = tmgmt_translator_ui_controller($service['translator']->plugin);
          $form = $ui_controller->checkoutSettingsForm($form, $form_state, $job);
        }
      }
    }
    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * Implements TMGMTTranslatorUIControllerInterface::checkoutInfo().
   */
  public function checkoutInfo(TMGMTJob $job) {
    $active_services = workflow_translator_get_active_services($job->getTranslator());
    $form = array();
    foreach ($active_services as $services) {
      foreach($services as $key => $service) {
        if (isset($service['translator']) && !empty($service['translator']->plugin)) {
          /** @var $ui_controller TMGMTTranslatorUIController */
          $ui_controller = tmgmt_translator_ui_controller($service['translator']->plugin);
          $form = array_merge($form, $ui_controller->checkoutInfo($job));
        }
      }
    }
    return $form;
  }


}
