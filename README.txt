SUMMARY
=======

Workflow translator plugin for the Translation Management Tools (TMGMT) project.
Allows to use additional services before and after a translation process.

Features:

* Create multiple Workflows
* For each workflow select translators and services, which should be run before
  or after a translation
* Use advanced translation jobs management tool to submit and review
  translations
* Interface to create own services

The project of course also supports implicitly all the features which are
provided by TMGMT like a feature-rich review process, being able to translate
different sources and more.

Known Service Implementations
=============================

* Enrycher API Integration - http://drupal.org/sandbox/kfritsche/1966286
  Allows to automatically enrich TMGMT Jobs with Text Analysis meta data
  (International Tag Set 2.0)

REQUIREMENTS
============

Depends on Translation Management Tool (tmgmt)
  http://drupal.org/project/tmgmt

INSTALLATION
============

* Install the required module (Translation Management Tool) as usual
  (http://drupal.org/node/70151).

* Install this module as usual, see http://drupal.org/node/70151
  for further information.

CONTACT
=======

Current maintainer:
* Karl Fritsche (kfritsche) - http://drupal.org/user/619702

This project has been sponsored by:
* Cocomore AG
  http://www.cocomore.com

* MultilingualWeb-LT Working Group
  http://www.w3.org/International/multilingualweb/lt/

* European Commission - CORDIS - Seventh Framework Programme (FP7)
  http://cordis.europa.eu/fp7/dc/index.cfm
