<?php

interface WorkflowTranslationDefaultServiceInterface extends TMGMTPluginBaseInterface {

  public function start(TMGMTJob $job);

  public function resume(TMGMTJob $job);

  public function getSupportedTargetLanguages($source_language);

  public function isAvailable();

}

abstract class WorkflowTranslationDefaultService extends TMGMTPluginBase implements WorkflowTranslationDefaultServiceInterface {

  public function start(TMGMTJob $job) {

  }

  public function resume(TMGMTJob $job) {

  }

  public function getSupportedTargetLanguages($source_language) {
    $languages = entity_metadata_language_list();
    unset($languages[LANGUAGE_NONE], $languages[$source_language]);
    return drupal_map_assoc(array_keys($languages));
  }

  public function isAvailable() {
    return TRUE;
  }

}

interface WorkflowTranslationDefaultUIServiceInterface extends TMGMTPluginBaseInterface {

  public function settingsForm($form, &$form_state, TMGMTTranslator $translator);

}

abstract class WorkflowTranslationDefaultUIService extends TMGMTPluginBase implements WorkflowTranslationDefaultUIServiceInterface {

  public function settingsForm($form, &$form_state, TMGMTTranslator $translator) {
    $form['markup'] = array(
      '#type'   => 'item',
      '#markup' => t('There are no settings for !service service.', array('!service' => $this->pluginInfo['label'])),
    );
    return $form;
  }

}

