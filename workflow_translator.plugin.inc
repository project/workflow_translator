<?php

/**
 * @file
 * Provides Workflow Translator plugin controller.
 */

/**
 * Workflow translator plugin controller.
 */
class WorkflowPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator, $returnList = FALSE) {
    $active_services = workflow_translator_get_active_services($translator);
    // no translator added
    if (empty($active_services[WORKFLOW_TRANSLATOR_TRANSLATOR]) || count($active_services[WORKFLOW_TRANSLATOR_TRANSLATOR]) == 0) {
      return FALSE;
    }
    $is_available = TRUE;
    $list = array();
    foreach ($active_services as $services) {
      foreach($services as $key => $service) {
        if (!empty($service['translator'])) {
          $service_available = $service['translator']->isAvailable();
        }
        else {
          $class = _tmgmt_plugin_controller('workflow', $key, 'plugin');
          $service_available = $class->isAvailable();
        }
        if (!$service_available) {
          $is_available = FALSE;
          if (!$returnList) {
            break 2;
          }
          else {
            $list[] = $service;
          }
        }
      }
    }
    if ($returnList) {
      return $list;
    }
    return $is_available;
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(TMGMTJob $job) {

    $current_service = $job->getSetting('currentService');

    $active_services = workflow_translator_get_active_services($job->getTranslator());

    // loop through all items
    foreach ($active_services as $services) {

      foreach($services as $key => $service) {

        // if called a second time, don't start from the beginning, start from last
        if (!isset($current_service)) {
          $current_service = $key;
        }
        else {
          if ($current_service == $key) {
            unset($current_service);
            if (!isset($service['translator'])) {
              // get controller of process
              /** @var $class WorkflowTranslationDefaultServiceInterface */
              $class = _tmgmt_plugin_controller('workflow', $key, 'plugin');
              if (method_exists($class, 'resume')) {
                $continue = $class->resume($job);
              }
              else {
                $continue = TRUE;
              }
              if (isset($continue) && $continue === FALSE) {
                $this->addJobToQueue($job);
              }
            }
            $job->addMessage('%service finished processing.', array('%service' => $service['label']));
          }
          continue;
        }

        $continue = TRUE;
        if (isset($service['translator'])) {
          $job->settings += $service['translator']->settings;
          $service['translator']->getController()->requestTranslation($job);
          // if one item is not yet set to needs review, then translator is not finished yet
          foreach ($job->getItems() as $job_items) {
            /** @var $job_items TMGMTJobItem */
            if (!$job_items->isNeedsReview()) {
              $continue = FALSE;
            }
          }
        }
        else {
          // get controller of process
          /** @var $class WorkflowTranslationDefaultServiceInterface */
          $class = _tmgmt_plugin_controller('workflow', $key, 'plugin');
          $continue = $class->start($job);
          if (!isset($continue)) {
            $continue = TRUE;
          }
        }

        $job->settings['currentService'] = $key;
        $job->save();

        if (!$continue) {
          $job->addMessage('%service started processing, waiting for response.', array('%service' => $service['label']));
          $this->addJobToQueue($job, isset($service['translator']));
          break 2;
        }
        else {
          $job->addMessage('%service finished processing.', array('%service' => $service['label']));
          unset($current_service);
        }

      }
    }
  }

  private function addJobToQueue(TMGMTJob $job, $translator = FALSE) {
    $queue_item = new stdClass();
    $queue_item->job = $job->tjid;
    $queue_item->translator = $translator;
    /** @var DrupalQueueInterface $queue */
    $queue = DrupalQueue::get('workflow_translator');
    $queue->createItem($queue_item);
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   */
  public function getSupportedTargetLanguages(TMGMTTranslator $translator, $source_language) {
    $languages = array();
    $active_services = workflow_translator_get_active_services($translator);
    foreach ($active_services as $services) {
      foreach($services as $key => $service) {
        if (!empty($service['translator'])) {
          $service_languages = $service['translator']->getSupportedTargetLanguages($source_language);
        }
        else {
          $class = _tmgmt_plugin_controller('workflow', $key, 'plugin');
          $service_languages = $class->getSupportedTargetLanguages($source_language);
        }
        if (empty($languages)) {
          $languages = $service_languages;
        }
        else {
          $languages = array_intersect($languages, $service_languages);
        }
      }
    }
    return $languages;
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::hasCheckoutSettings().
   */
  public function hasCheckoutSettings(TMGMTJob $job) {
    return TRUE;
  }

}
